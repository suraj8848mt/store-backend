const router = require('express').Router();
const Product = require('../models/product');
const load = require('../middleware/multer');
const cloudinary = require("cloudinary")
require("../middleware/cloudinary")



// Get Request
router.get('/products', async (req, res)=>{
    try {
        let product = await Product.find().populate('owner category').exec();
        res.json({
            success: true,
            product: product
        });

    } catch (err) {
        res.status(500).json({
            success: false,
            message: err.message
        })
    }
})


// Post request - create a new product
router.post("/products",load.single('photo'), async (req, res) => {
    const result = await cloudinary.v2.uploader.upload(req.file.path)
    try {
        let product = new Product();
        product.owner = req.body.ownerID;
        product.category = req.body.categoryID;
        product.price = req.body.price;
        product.title = req.body.title;
        product.description = req.body.description;
        product.photo = result.secure_url;
        product.stockQuantity = req.body.stockQuantity;

        await product.save();

        res.json({
            status: true,
            message: "saved successfully"

        });
    } catch (err) {
        res.status(500).json({
            success: false,
            message: err.message
        });
    }
});


// PUT Request
router.put("/products/:id", load.single('photo'), async (req, res)=> {
    const result = await cloudinary.v2.uploader.upload(req.file.path)
    try {
        let product = await Product.findOneAndUpdate(
            { _id: req.params.id }, {
            $set: {
                title: req.body.title,
                price: req.body.price,
                category: req.body.categoryID,
                photo: result.secure_url,
                description: req.body.description,
                owner: req.body.ownerID

            }
        },
        {upsert: true}
        );
        res.json({
            success: true,
            UpdatedProduct: product
        });
    } catch (err) {
        res.status(500).json({
            success:false,
            message: err.message
        });
    }
});


// GET Request- get a single product by id
router.get("/products/:id", async (req, res) => {
    
    try {
        let product = await Product.findOne({_id: req.params.id}).populate('owner category').exec();

        res.json({
            success: true,
            product: product
        })
    } catch (err) {
        res.status(500).json({
            success: false,
            message: err.message
        });
        
    }
});


// Delete Request - delete a single product
router.delete("/products/:id", async(req, res) =>{
    try { 
        let deleteProdut = await Product.findOneAndDelete({_id: req.params.id});
        if (deleteProdut){
            res.json({
                status: true,
                message: "Successfully Deleted"
            });
        }
    } catch (err) {
        res.status(500).json({
            success: false,
            message: err.message
        })
    }
})

module.exports = router;