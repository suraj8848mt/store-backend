const router = require("express").Router();
const Owner = require("../models/owner");
const load = require("../middleware/multer");
const cloudinary = require("cloudinary");
require("../middleware/cloudinary");

// POST API
router.post("/owners", load.single("photo"), async (req, res) => {
    const result = await cloudinary.v2.uploader.upload(req.file.path);
    try {
        let owner = new Owner();
        owner.name = req.body.name;
        owner.about = req.body.about;
        owner.photo = result.secure_url;

        await owner.save();

        res.json({
            success: true,
            message: "Successfully created owner",
        });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err.message,
        });
    }
});

// Get Request
router.get("/owners", async (req, res) => {
    try {
        let owners = await Owner.find();
        res.json({
            success: true,
            owners: owners,
        });
    } catch (err) {
        res.status(500).json({
            success: false,
            message: err.message,
        });
    }
});

module.exports = router;