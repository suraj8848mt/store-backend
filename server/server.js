const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const dotEnv = require("dotenv");
const cors = require("cors");
const User = require("./models/user");

require("./middleware/cloudinary");

dotEnv.config();

const app = express();

mongoose.connect(
  process.env.DATABASE,
  { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true },
  (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("Database connected ....");
    }
  }
);

// Middleware
app.use(cors());
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// require apis

const productRoutes = require("./routes/product");
app.use("/api", productRoutes);

const categoryRoutes = require("./routes/category");
app.use("/api", categoryRoutes);

const ownerRoutes = require("./routes/owner");
app.use("/api", ownerRoutes);

const authRouter = require("./routes/auth");
app.use("/api", authRouter);

app.listen(8000, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("Port Running At:", 8000);
  }
});
